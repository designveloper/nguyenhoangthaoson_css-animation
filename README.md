## 1. Overview of CSS Transitions and Transform
    * CSS transform basic
        - Changes the shape and position of the affected content by modifying the coordinate space
        - Do not interrupt the normal document flow


## 2. CSS Animations
    * 2 step:
        - Define animations
        - Assign animation to specific element(s)
    * @keyframe block
        - Keyframes are a list describing what should happen over the course of the animation.
## 3. CSS Animation Building Blocks
    * animation-play-state:  starts/pauses animation
## 4. Applying CSS Animation to SVG
    * Reason to choose SVG:
        - Resolution independent
        - Small file size   
        - Accessiblity
    * All tips:
        - Use independent shapes:
            + Avoid merging path
            + Layer shapes, avoid knocking out of the background shapes
        - Keep drawing simple
            + Saves file size
            + Means easier animation
        - Use simple shapes instead of path

## 5. Performance, browser support & fallbacks
    * When to use:
        - CSS keyframe animation:
            + Demonstrations or linear animations
            + Very simple state-based animation        
        - CSS transitions:
            + Toggling between two states
            + Animating on :hover, :active, :checked
    * Most performant properties
        - Opacity
        - Scale, rotation, position with transform        
    * The 4 steps browsers have to go through to update changes on the page:
        - Style: Calculating the style to apply
        - Layout: Generating the geometry and position of each element
        - Paint: Filling out the pixels for each element into the layers that make up the page
        - Composite: Drawing out the layers onto the screen    
    * The translate 3D "hack"
        - Turns on hardware acceleration
        - Promotes objects to a new layer -> forces browsers to increase performance
        - Transform: translated(0, 0, 0)   
    * Code organization
        - Keep object specific animation code with the object's other styles 
        - Centralize animation code that will be repurposed on multiple objects throughout a project    

## 6. Tools
    * Custome easing idea: 
        http://easings.net/
    * Cubic-bezier:
        http://cubic-bezier.com/
    * Make Complex animation:
        http://bouncejs.com/
    * Advance svg animation:
        https://greensock.com/    
        http://snapsvg.io/
    
    * SASS:
        http://thesassway.com/           
         